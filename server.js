 const express=require('express');
 const app=express();
 const mongoose=require('mongoose');

 app.use(express.json());
 
 //connect db
const connectDB = require("./config/db");
connectDB();

//Middleware
app.use('/post',()=>{
    console.log('We are on Middleware');
})

//Import roues
const blogRoutes=require('./routes/blogs');
app.use('/blogs',blogRoutes);

// app.get('/',(req,res)=>{
//     res.send('We are on home');
// });

 app.listen(3000,()=>{
     console.log('listening on port 3000');
 });