const express=require('express');
const router=express.Router();

const Blog = require('../models/Blog')

//Get blog of an user
router.get('/:Id', async (req,res)=>{
    try{
     const blog=Blog.findById(req.params.Id);
     res.json(post);
    }
    catch{
        res.json({message:err});
    }
});

//Get all blogs
router.get('/',async (req,res)=>{
    try{
       const blogs = await Blog.find();
       res.json(blogs)
    }
    catch(err)
    {
        res.json({message:err});
    }
});


//Delete Blog
router.delete('/:authorId',async (req,res)=>{
    try{
    const removedBlog= Blog.remove({_id: req.params.authorId});
    res.json(removedBlog);
}
    catch(err){
        res.json({message:err});
    }

});

//Update a blog
router.put('/:Id', async (req,res)=>{
    try{
        const id=req.params.Id;
        let user=await Blog.findById(id);
        user.description=req.body.description;
        await user.save();
        res.status(200).json(user);
    }
    catch(err){
        res.json({message:err});
    }
});

//Insert a blog with given id
router.post('/', async (req,res)=>{
    const blog=new Blog({
       topic:req.body.topic,
       description:req.body.description,
       date:req.body.date,
       authorId:req.body.authorId,
       authorName:req.body.authorName
    });
   
    try{
    const savedBlog = await blog.save();
    res.json(savedBlog);
    }
    catch(err){
        res.json({message:err});
    }


});

module.exports=router;