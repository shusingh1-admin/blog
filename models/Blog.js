const mongoose=require('mongoose');

const BlogSchema = mongoose.Schema({
    topic:{
        type: String,
        required : false
    },
     description:{
         type: String,
         required: false
     },
     date:{
         type: Date,
         default: Date.now
     },
     authorId:{
         type: Number,
         required: true
     },
     authorName:{
         type: String,
         required : true
     }

});


module.exports=mongoose.model('Blogs',BlogSchema);